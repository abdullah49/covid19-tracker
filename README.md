# Covid-19-tracker Docker

Steps needs to follow for Development environment:

1. docker build . -t covid-19-tracker

2. docker run --rm -p 3000:3000 -v $(pwd):/code --name covid-19-app -t -d covid-19-tracker

[Optional] Check your container has been creted or not using docker ps -a

3. docker exec -it covid-19-app bash

4. npm start

Happy Coding :)
